---
title: About me
date: 2018-12-30T14:30:00.000+00:00
---

I am Rohit a student from India.

My primary interests are: Blogging, learning English, following technology news, renovating my desk setup, writing, learning new things.

Activities I spend most time on: Writing articles, following latest technology (my personal favorite media is _The Verge_), listening music (love spotify), reading about  **BCI** (Brain Computer Interface), sketching and doodling.

(NOTE: I am new to blogging and want to learn more in the language of English and its vocabulary, so I wish you may help me in improving by saying my mistakes to me by mailing or tweeting.)

Go checkout my articles!
