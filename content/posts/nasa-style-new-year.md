---
title: NASA style New Year 2019
date: 2019-01-01 21:00:00 +0530
description: A successful flyby by NASA's New Horizon!
categories:
- Science
- Astronomy
tags:
- NASA
- Space
- Dwarf Planet
cover: "/images/cover2.jpg"
---

On the eve of New Year-2019, NASA's New Horizon spacecraft did something amazing. Let me tell you more about it. But before that,

## What is NASA?

NASA (National Aeronautics and Space Administration) was established in 1958. NASA's HQ is located in Washington DC, U.S.A. This agency's primary aim is to observe and obtain a good understanding of the Earth and the space surrounding it. Over the years, NASA has sent lots of spacecrafts to space. One of them is the 'New Horizon'.

![](/images/1.jpg)

## What is the New Horizon spacecraft?

The new horizon was a spacecraft launched by NASA on behalf of the 'New Frontiers Program' on 19th of January 2006 at 'Cape Canaveral Air Force Zone' by Atlas V Rocket. Its main motto is to have a 'flyby' from Earth to Pluto and then to Kuiper belt object. Here, flyby means the process of going near and examining a planet or a dwarf planet closely for scientific data. On 25th of October 2016, almost 10 years after the launch, the New Horizon achieved a flyby to a dwarf planet, Pluto. And today i.e., January 1st of 2019 at 12:33 EST it marvelously achieved another flyby through a Kuiper Belt Object name 'Ultima Thule'.

![](/images/2.png)

## What are Kuiper Belt Objects (KBO)?

The Kuiper belt, sometimes also called as 'edge worth-Kuiper belt', is a set of rocks (and stuff) looking like a circular ring covering our solar system starting from the orbit of the planet 'Neptune'. Pluto belongs in there and is also a Kuiper belt object. There are nearly 35 thousand KBOs with an average diameter of 100 km and nearly 1 million with a diameter of  20 km.

![Attribution: WilyD at English Wikipedia](/images/kupier belt.png)

## What is the object that NASA recently flyby in 2019?

When the world is celebrating the new year of 2019, the scientists working on New Horizon from NASA and JHU are full of stress. Only 30 minutes after the year started they started celebrating, for the successful flyby through 'Ultima Thule' which is situated in Kuiper belt, nearly 6.6 billion kilometers away from our planet Earth.

![Credit: Adrian Mann/All About Space](/images/ultima thule.jpg "This is Ultima Thule which looks like a giant peanut to me.")

And according to me, NASA's achievement of having a flyby through Ultima Thule is far more celebratory than the Earth finishing one more rotation around the Sun (i.e., New Year!).

Congratulation to everyone involved in the New Horizon!

## Credits for the information

* [theplanets.org/kuiper-belt/](theplanets.org/kuiper-belt/)
* [wikipedia.org/wiki/new_horizon](wikipedia.org/wiki/new_horizon)
* [nasa.gov](nasa.gov)
* [space.com/42871-new-horizon-ultima-thule-flyby-success.html](space.com/42871-new-horizon-ultima-thule-flyby-success.html)
* [wikipedia.org/wiki/nasa](wikipedia.org/wiki/nasa)
