---
title: Human Evolution
date: 2018-12-31T14:30:00.000+00:00
tags:
- human
- evolution
categories:
- science
- biology
description: Stages of human evolution from apes.
cover: "/images/4.jpg"

---
We  humans are technically called **_homo sapiens_**. We evolved out of the species **_apes_**. 

![](/images/pic 3.jpg)

'Charles Darwin' is the first to ideate an evolution theory describing stages from micro organisms to present forms of life.

Currently humans are the most intelligent organisms living on this planet Earth. They discovered and invented many things until today and will discover more tomorrow.

We have evolved from the species called 'apes' which are living on this planet for nearly 8.6 million years.

These slowly evolved into **_Orrorin Tugenensis_**  which used to live nearly 5.8 million years ago which were the first mammals to walk on 2 legs i.e., bi- arthropods.

**Fact:**_the volume of human brain is 1500 cc(cubic centimeters)_

Slowly **_Australopithecus_** have evolved nearly 5.5 million years ago ,who behave similar as today's chimpanzees.

After 1.5 million years from there, **_Australopithecus_** were evolve with better brain volume of around 400-500 cc.

Around 3.2 million years ago a species evolved with the name **_Australopithecus Afarensis._** geologists found a fossil of it named Lucy was the most famous fossil of this whole species.

At 2.7 million year ago a species have evolved and changed the food style completely that they started vegetation, and hence started the class of vegetation. this species is named as **_Parathropus._**

Slowly evolution is getting complicated  .....

At around 2.5 million years ago the species named **_homo habilis_** gave a big flip for the evolution, they discovered **fire** which gave us all the yummy food what we are eating today.they first discovered foo by creating friction between two stones.

Closely 2.0 million years ago the species called **_homo eraster_** had nearly double the brain volume of **_Australopithecus_** that is 800-900  cc.

Now a new species have came and occupied for nearly 3 centuries named **_Homo Heidelbergensis_** having the best brain volume of 1200 cc have evolved at around 600 thousand years ago. nearly at 500 K years ago they started building their shelter and at 400 K years ago they started to hunting with spheres.

Evolution is getting complicated, **_neondertals_** have evolved nearly 230 k years ago who started making different tools with the help of stones.

![](/images/pic 1.jpg)

We the **_Homo Sapiens_** have evolved on Earth. at 195 K years ago with the brain volume of 1350 cc , slowly evolution in moving and humans got the brain volume of 1500 cc.

**_WHAT HOMO SAPIENS DID:_**

* Nearly 150 K years ago we started making Jewelry.
* We found theory of evolution.
* We found electricity through different sources.
* We found many inventions and discoveries.
* We created civilization.
* W pollute atmosphere.
* We do mistakes knowingly.
* We found physical and biological laws.
* W started discrimination.
* We are those who can stop discrimination,pollution,dong mistakes knowingly,save equality.
* **_Humans are they who can be a role model for next species of evolution or robots._**

![](/images/g.jpg)

Hello readers, this is my first article. Please help me improve. Please let me know your suggestions and corrections by commenting below or [mailing me](mailto:rohit@infoyard.me).