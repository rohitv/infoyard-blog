---
title: Morse code an intresting language
date: 2019-07-15 21:00:00 +0530
description: This article specifies you about our Morse code
---

Morse code is introduced by Samuel F B Morse after the death of his wife as a fastest way to transport a message to other person in a easy way.

In his middle age Morse contributed to the [invention](https://en.wikipedia.org/wiki/Invention) of a single-wire [telegraph](https://en.wikipedia.org/wiki/Electric_telegraph#Morse_System) system based on European telegraphs. He was a co-developer of [Morse code](https://en.wikipedia.org/wiki/Morse_code) and helped to develop the commercial use of telegraphy.

Morse code is a binary session with a new type of 0's and 1's an dots(.) and dashes(-).

This session of binary is used in sending someone a secret message in the form of dots and dashes, different letter has different combinations as

A .-

B -..

C -.-.

D -..

E .

F ..-.

G --.

H ....

I ..

J .----

K -.-

L .-..

M --

N -.

O ---

P .--.

Q --.-

R .-.

S ...

T -

U ..-

V ...-

W .--

X -..-

Y -.--

Z --..

there are few rules o be taken into consider as a dot calls a single beep line and a dash calls a triple beep line and a space between words has 7 beep lines and a space of letter end to letter start has a single beep line, bit complicated.

Ex:

'HI U' is the sentence I choose and I take a single beep line as space in the beeps

Morse code is: . . . . . . . . - this is the particular code for the sentence hi u.

As if in the forms of 0's and 1's we should follow through binary you can consider that 0 represents to space

dot as 1

dash as 111

space between beep line 0

space between letters 000

space between words 0000000

As if we can consider these binary codes in numerically as:

A - 10111  
B- 1110101

C- 11101011101

D- 1110101

E-1

F- 101011101

G- 111011101

H- 1010101

I- 101

J- 1011101110111

K- 111010111

L- 101110101

M- 1110111

N- 11101

O- 11101110111

P- 10111011101

Q- 1110111010111

R- 1011101

S- 10101

T- 111

U- 1010111

V- 101010111

W- 101110111

X- 11101010111

Y- 1110101110111

Z - 11101110101

Ex: HI U

1010101010100000001010111

Morse code is highly used at the time of world war 2 for sending secret messages to the solders by using torch lights in SOS mode or blinking the eyes in a way which shows long and short beep lines.

(Students now-a-days also using this Morse code to copy in exams by clicking their pens.)

I like to use this Morse code to save my secret data safely and have a secret conversation with my team mates.

Thanks to: sources are:

<[https://en.wikipedia.org/wiki/Samuel_Morse](https://en.wikipedia.org/wiki/Samuel_Morse "https://en.wikipedia.org/wiki/Samuel_Morse")>