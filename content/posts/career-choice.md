---
date: 2019-06-09 00:00:00 +0530
description: This may help you come out of ur massive confusion on choosing right path to move onn in your life.
title: Career choice
---

Choosing the right career is not an easy task, there will be a lot of confusion regarding moving in the right way, after grade 10th in India we get stopped in choosing the appropriate +2 course. So, I would like to share few tips to get an understanding about your career after grade 10.

Firstly ask yourself a question:  WHAT AM I GOOD AT? It may be a bit hard to you, if you are not able to solve it then try: WHAT PEOPLE SAY I AM GOOD AT? You may get an answer.

Now solve a puzzle given by my dad "It's your favourite movie for which you have been waiting for months to get released, the time has come... The movie got released and only one seat is available in the first row of the theatre in a corner and near the door where people move around, but it's your favourite movie, would you choose that seat or wait for the next show and have a better seat with good view of the movie?" Answer to this puzzle says your future.

If you chose the seat at the corner of the theatre it will be hard to see the movie but you can see it first, similarly in your life too. If you chose short and faster studies you may have a corner and non enjoyable life but if you wait until the next show of the movie you will have a perfect view of movie.

Choice is yours. Wheather you would like to have an enjoyable or non-enjoyable life it's in your hand. My suggestion is to wait until the fruit ripes and then have its sweetness as  you wait until the next show of the movie and have a good view of it and _wait to complete your studies and have an enjoyable life_. Choice is yours.

From the above questions you may come to a conclusion in your career choice, whatever it is please do not follow anything by others, pick your own career which suits you better, be patient, think hard, work hard and go for a best option.

Here are a few fields I know and suggest people:

Animation

Science

Engineering

Medical

Computer & IT

Management

Mass Communication

Law

Armed Forces

Humanities

Commerce

Vocational Courses

Performing Art

Designing

Professional Courses

10th & 12th professer

Agriculture. Sci. Tech.

Miscellaneous Businesses

There a many other options to choose, but choose one which suits you and the one which you enjoy to do.
